SRC=logger.c service.c print_jobs.c
OBJ=$(SRC:.c=.o)
EXE=printlog.exe

CC=i686-w64-mingw32-gcc
STRIP=i686-w64-mingw32-strip
CFLAGS=-Wall -O3 -Iinclude -DCURL_STATICLIB -g
LDFLAGS= -mconsole -mwindows -lgdi32  -lwinspool -Llib -lcurl -lrtmp -lwinmm -lidn -lssh2 -lssl -lz  -lcrypto -lws2_32 -lwldap32 -lrpcrt4
RM=rm

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

.PHONY : all
all: $(EXE)
	@$(STRIP) $(EXE)

$(EXE): $(OBJ)
	$(CC) $(OBJ) $(LDFLAGS) -o $@

.PHONY : clean
clean:
	-$(RM) $(OBJ) $(EXE) 

reformat:
	indent -kr --no-tabs --braces-on-if-line --braces-on-func-def-line --line-length120 *.c *.h
	@-$(RM) *~
	
