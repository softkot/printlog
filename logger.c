#include <stdio.h>
#include <windows.h>
#include <winspool.h>
#include <rpc.h>
#include <curl/curl.h>
#include "logger.h"

#define POST_URL "https://10.149.0.254/printlog/"
#define MONITOR_JOB (PRINTER_CHANGE_ADD_JOB | PRINTER_CHANGE_SET_JOB | PRINTER_CHANGE_DELETE_JOB | PRINTER_CHANGE_WRITE_JOB)
#define MONITOR_PRINTERS (PRINTER_CHANGE_ADD_PRINTER | PRINTER_CHANGE_SET_PRINTER | PRINTER_CHANGE_DELETE_PRINTER)

WORD JobFields[] = {
    JOB_NOTIFY_FIELD_PRINTER_NAME,
    JOB_NOTIFY_FIELD_MACHINE_NAME,
    JOB_NOTIFY_FIELD_PORT_NAME,
    JOB_NOTIFY_FIELD_USER_NAME,
    JOB_NOTIFY_FIELD_NOTIFY_NAME,
    JOB_NOTIFY_FIELD_DATATYPE,
    JOB_NOTIFY_FIELD_PRINT_PROCESSOR,
    JOB_NOTIFY_FIELD_PARAMETERS,
    JOB_NOTIFY_FIELD_DRIVER_NAME,
    JOB_NOTIFY_FIELD_DEVMODE,
    JOB_NOTIFY_FIELD_STATUS,
    JOB_NOTIFY_FIELD_STATUS_STRING,
    JOB_NOTIFY_FIELD_DOCUMENT,
    JOB_NOTIFY_FIELD_PRIORITY,
    JOB_NOTIFY_FIELD_POSITION,
    JOB_NOTIFY_FIELD_SUBMITTED,
    JOB_NOTIFY_FIELD_START_TIME,
    JOB_NOTIFY_FIELD_UNTIL_TIME,
    JOB_NOTIFY_FIELD_TIME,
    JOB_NOTIFY_FIELD_TOTAL_PAGES,
    JOB_NOTIFY_FIELD_PAGES_PRINTED,
    JOB_NOTIFY_FIELD_TOTAL_BYTES,
    JOB_NOTIFY_FIELD_BYTES_PRINTED
};

WORD PrinterFields[] = {
    PRINTER_NOTIFY_FIELD_COMMENT,
    PRINTER_NOTIFY_FIELD_LOCATION
};

PRINTER_NOTIFY_OPTIONS_TYPE Notifications[] = {
    {
     JOB_NOTIFY_TYPE,
     0,
     0,
     0,
     sizeof(JobFields) / sizeof(WORD),
     JobFields}
    ,
    {
     PRINTER_NOTIFY_TYPE,
     0,
     0,
     0,
     sizeof(PrinterFields) / sizeof(WORD),
     PrinterFields}
};

PRINTER_NOTIFY_OPTIONS NotificationOptions = {
    2,
    0,
    sizeof(Notifications) / sizeof(PRINTER_NOTIFY_OPTIONS_TYPE),
    Notifications
};

char buf[4096];
DWORD reload_printers = 0;

size_t dumb_write(char *ptr, size_t size, size_t nmemb, void *userdata) {
    return nmemb * size;
}

void reloadPrintersInfo() {
    reload_printers = TRUE;
}

void updateJobFromDeviceMode(PRINT_JOB * job, PDEVMODE devmode) {
    if (devmode->dmFields & DM_COLLATE) {
        replace_job_param(job, "JOB_COLLATE_COPIES", devmode->dmCollate == DMCOLLATE_TRUE ? "YES" : "NO");
    }
    if (devmode->dmFields & DM_DUPLEX) {
        char *duplex_mode;
        switch (devmode->dmDuplex) {
        case DMDUP_HORIZONTAL:
            duplex_mode = DUP_HORIZONTAL;
            break;
        case DMDUP_VERTICAL:
            duplex_mode = DUP_VERTICAL;
            break;
        default:
            duplex_mode = DUP_SIMPLEX;

        }
        replace_job_param(job, "JOB_DUPLEX", duplex_mode);
    }

    if (devmode->dmFields & DM_COLOR) {
        replace_job_param(job, "JOB_COLOR", (devmode->dmColor == DMCOLOR_COLOR) ? "YES" : "NO");
    }

    if (devmode->dmFields & DM_COPIES) {
        replace_job_param_int(job, "JOB_COPIES", devmode->dmCopies);
    }
}

void reportJobsToRemoteHost() {
    PRINT_JOB *job = print_jobs;

    while (job) {
        PRINT_JOB *next = job->next;
        if (job->finished) {
            replace_job_param(job, LOG_STATUS, LOG_STATUS_FINAL);
        }

        if (job->changed || job->finished) {
            report_print_job(job);
        }

        if (job->finished) {
            delete_print_job(job);
        }
        job = next;
    }
}

void updatePrinterJobsInfo(WORD whatsChanged, PRINTER_OBJECT printer, PPRINTER_NOTIFY_INFO notifyInfo) {
    //printf("Change updates %#08x with %ld attrs\n", whatsChanged, notifyInfo == NULL ? 0 : notifyInfo->Count);

    if (whatsChanged & (PRINTER_CHANGE_ADD_PRINTER | PRINTER_CHANGE_DELETE_PRINTER)) {
        reloadPrintersInfo();
    }

    if (notifyInfo && (notifyInfo->Count > 0)) {
        int i;
        for (i = 0; i < notifyInfo->Count; i++) {
            PRINTER_NOTIFY_INFO_DATA notification = notifyInfo->aData[i];
            //printf("Notify type %#08x field %#08x\n", notification.Type, notification.Field);

            if (whatsChanged & MONITOR_PRINTERS) {
                if (notification.Type == PRINTER_NOTIFY_TYPE) {
                    if (notification.Field == PRINTER_NOTIFY_FIELD_COMMENT) {
                        reloadPrintersInfo();
                    }

                    if (notification.Field == PRINTER_NOTIFY_FIELD_LOCATION) {
                        reloadPrintersInfo();
                    }
                }
            }

            if ((notification.Type == JOB_NOTIFY_TYPE) && (whatsChanged & MONITOR_JOB)) {
                PRINT_JOB *job = get_print_job(notification.Id);

                if (job->new) {
                    replace_job_param(job, "JOB_PRINTER_LOCATION", printer.location);
                    replace_job_param(job, "JOB_PRINTER_COMMENTS", printer.comments);
                }

                if (whatsChanged & PRINTER_CHANGE_DELETE_JOB) {
                    job->finished = TRUE;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_PRINT_PROCESSOR) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_PRINT_PROCESSOR", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_PARAMETERS) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_PARAMETERS", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_PORT_NAME) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_PORT_NAME", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_DRIVER_NAME) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_DRIVER_NAME", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_SUBMITTED) {
                    unsigned long long ticks = 0;
                    PFILETIME ft = (PFILETIME) & ticks;;
                    SystemTimeToFileTime((PSYSTEMTIME)
                                         notification.NotifyData.Data.pBuf, ft);
                    ticks = ticks / WINDOWS_TICK - SEC_TO_UNIX_EPOCH;
                    snprintf(buf, sizeof(buf), "%I64d", ticks);
                    replace_job_param(job, "JOB_NOTIFY_FIELD_SUBMITTED", buf);
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_PRINTER_NAME) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_PRINTER_NAME", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_MACHINE_NAME) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_MACHINE_NAME", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_USER_NAME) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_USER_NAME", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_DOCUMENT) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_DOCUMENT", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_DATATYPE) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_DATATYPE", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_STATUS_STRING) {
                    replace_job_param(job, "JOB_NOTIFY_FIELD_STATUS_STRING", (PCHAR)
                                      (notification.NotifyData.Data.pBuf));
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_DEVMODE) {
                    PDEVMODE devmode = (PDEVMODE) (notification.NotifyData.Data.pBuf);
                    updateJobFromDeviceMode(job, devmode);
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_TOTAL_BYTES) {
                    replace_job_param_int(job, "JOB_NOTIFY_FIELD_TOTAL_BYTES", notification.NotifyData.adwData[0]);
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_TOTAL_PAGES) {
                    replace_job_param_int(job, "JOB_NOTIFY_FIELD_TOTAL_PAGES", notification.NotifyData.adwData[0]);
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_PAGES_PRINTED) {
                    replace_job_param_int(job, "JOB_NOTIFY_FIELD_PAGES_PRINTED", notification.NotifyData.adwData[0]);
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_BYTES_PRINTED) {
                    replace_job_param_int(job, "JOB_NOTIFY_FIELD_BYTES_PRINTED", notification.NotifyData.adwData[0]);
                    continue;
                }

                if (notification.Field == JOB_NOTIFY_FIELD_STATUS) {
                    replace_job_param_int(job, "JOB_NOTIFY_FIELD_STATUS", notification.NotifyData.adwData[0]);
                    continue;
                }
            };
        }
    }
}

int isSignaled(HANDLE handle) {
    return (handle != INVALID_HANDLE_VALUE) && (WaitForSingleObject(handle, 0U) == WAIT_OBJECT_0);
}

int monitorPrinters(HANDLE stopevent) {
    DWORD printer_idx;
    DWORD printer_count;
    DWORD whatsChanged = 0;
    CURL *curl;
    PPRINTER_INFO_2 printers;
    int exitLoop = FALSE;
    DWORD bufsz;
    PRINTER_OBJECT *printer_handles;
    HANDLE *wait_handles;

    PPRINTER_NOTIFY_INFO notifyInfo = NULL;

    curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_URL, POST_URL);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, dumb_write);
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    init_print_jobs(curl);
    while (!exitLoop) {
        printf("Configure printer listners\n");
        EnumPrinters(PRINTER_ENUM_LOCAL, NULL, 2, NULL, 0, &bufsz, &printer_count);
        printers = malloc(bufsz);
        EnumPrinters(PRINTER_ENUM_LOCAL, NULL, 2, (LPBYTE) printers, bufsz, &bufsz, &printer_count);
        printer_count++;        // one more handle for global spooler handle
        int hcount = printer_count;
        printer_handles = (PRINTER_OBJECT *) calloc(printer_count, sizeof(PRINTER_OBJECT));
        wait_handles = (HANDLE *) calloc(printer_count + 1, sizeof(HANDLE));
        if (stopevent != INVALID_HANDLE_VALUE) {
            wait_handles[hcount] = stopevent;
            hcount++;
        }
        for (printer_idx = 0; printer_idx < printer_count; printer_idx++) {
            HANDLE pHandle;
            char *printerName = NULL;
            if (printer_idx > 0) {
                PPRINTER_INFO_2 printer = &printers[printer_idx - 1];
                printerName = printer->pPrinterName;
                printer_handles[printer_idx].comments = strdup(printer->pComment);
                printer_handles[printer_idx].location = strdup(printer->pLocation);
            }
            OpenPrinter(printerName, &pHandle, NULL);
            printer_handles[printer_idx].handle = pHandle;
            wait_handles[printer_idx] = FindFirstPrinterChangeNotification(pHandle,
                                                                           (printerName ==
                                                                            NULL) ? MONITOR_PRINTERS :
                                                                           MONITOR_JOB, 0, &NotificationOptions);
            printf("\tlistner for %s handle %ld notifier %ld\n", printerName,
                   (DWORD) printer_handles[printer_idx].handle, (DWORD) wait_handles[printer_idx]);
        }
        free(printers);

        printf("Wait for printer changes on %d handles\n", hcount);
        reload_printers = 0;
        while (!(exitLoop | reload_printers)) {
            WaitForMultipleObjects(hcount, wait_handles, FALSE, INFINITE);
            if (isSignaled(stopevent)) {
                exitLoop = TRUE;
                break;
            }
            for (printer_idx = 0; printer_idx < printer_count; printer_idx++) {
                HANDLE notifier = wait_handles[printer_idx];
                if (isSignaled(notifier)) {
                    notifyInfo = NULL;
                    NotificationOptions.Flags = 0;
                    FindNextPrinterChangeNotification(notifier, &whatsChanged, &NotificationOptions,
                                                      (LPVOID *) & notifyInfo);


                    if (notifyInfo && (notifyInfo->Flags & PRINTER_NOTIFY_INFO_DISCARDED) != 0) {
                        printf("Refresh info\n");
                        NotificationOptions.Flags = PRINTER_NOTIFY_OPTIONS_REFRESH;
                        FreePrinterNotifyInfo(notifyInfo);
                        FindNextPrinterChangeNotification(notifier,
                                                          &whatsChanged, &NotificationOptions, (LPVOID *) & notifyInfo);
                    }
                    updatePrinterJobsInfo(whatsChanged, printer_handles[printer_idx], notifyInfo);
                    if (notifyInfo) {
                        FreePrinterNotifyInfo(notifyInfo);
                        notifyInfo = NULL;
                    }
                }
            }
            reportJobsToRemoteHost();
        }
        for (printer_idx = 0; printer_idx < printer_count; printer_idx++) {

            if (printer_handles[printer_idx].comments) {
                free(printer_handles[printer_idx].comments);
            }

            if (printer_handles[printer_idx].location) {
                free(printer_handles[printer_idx].location);
            }

            FindClosePrinterChangeNotification(wait_handles[printer_idx]);
            ClosePrinter(printer_handles[printer_idx].handle);
        }
        free(printer_handles);
        free(wait_handles);
    }
    reportJobsToRemoteHost();
    curl_easy_cleanup(curl);
    return 0;
}
