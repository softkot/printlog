#include <curl/curl.h>

#define PRINTLOG_VERSION "1.17 (2014-02-11)"

#define WINDOWS_TICK 10000000LL
#define SEC_TO_UNIX_EPOCH 11644473600LL
#define LOG_STATUS "LOG_STATUS"
#define LOG_STATUS_UPDATE "UPDATE"
#define LOG_STATUS_FINAL "FINAL"

#define DUP_HORIZONTAL "HORIZONTAL"
#define DUP_VERTICAL "VERTICAL"
#define DUP_SIMPLEX "OFF"


typedef struct printer_object {
    char *comments;
    char *location;
    HANDLE handle;
} PRINTER_OBJECT;

typedef struct job_param {
    char *key;
    char *value;
    struct job_param *next;
} JOB_PARAM;


typedef struct print_job {
    unsigned int id;
    int changed;
    int finished;
    int new;
    struct print_job *prev;
    struct print_job *next;
    JOB_PARAM *params;
} PRINT_JOB;

void init_print_jobs(CURL * reporter);
JOB_PARAM *get_job_param(PRINT_JOB * job, char *param);
int replace_job_param(PRINT_JOB * job, char *key, char *value);
int replace_job_param_int(PRINT_JOB * job, char *key, DWORD value);
PRINT_JOB *get_print_job(unsigned int id);
int delete_print_job(PRINT_JOB * job);
void report_print_job(PRINT_JOB * job);

extern PRINT_JOB *print_jobs;
