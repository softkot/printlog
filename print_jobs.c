#include <stdio.h>
#include <windows.h>
#include <winspool.h>
#include <curl/curl.h>
#include <rpc.h>
#include "logger.h"

PRINT_JOB *print_jobs;
char spoolerDnsName[4096];
WCHAR buf_wide_char[4096];
char buf_utf8[4096];
char buf[4096];
CURL *curl;

void print_jobs_queue() {
    printf("Print jobs rqueue :: ");
    PRINT_JOB *res = print_jobs;
    while (res) {
        printf("%ld ", (DWORD) res);
        res = res->next;
    }
    printf("\n");
}

char *utf8(char *local_cp_str) {
    if (!local_cp_str) {
        return NULL;
    }
    MultiByteToWideChar(CP_ACP, 0, local_cp_str, -1, (LPWSTR) buf_wide_char, sizeof(buf_wide_char) / sizeof(WCHAR));
    WideCharToMultiByte(CP_UTF8, 0, buf_wide_char, -1, buf_utf8, sizeof(buf_utf8), NULL, NULL);
    return buf_utf8;
}

int replace_job_param(PRINT_JOB * job, char *key, char *value) {
    if ((value == NULL) || (strlen(value) == 0)) {
        return FALSE;
    }
    char *utf8_value = utf8(value);

    JOB_PARAM *res = get_job_param(job, key);
    if (res->value) {
        if (!strcmp(res->value, utf8_value)) {
            return FALSE;
        }
        free(res->value);
    }

    res->value = malloc(strlen(utf8_value) + 1);
    strcpy(res->value, utf8_value);
    job->changed = TRUE;
//printf("Set job (%d) : %s=%s\n",job->id,res->key,res->value);
    return TRUE;
}

int replace_job_param_int(PRINT_JOB * job, char *key, DWORD value) {
    snprintf(buf, sizeof(buf), "%ld", value);
    return replace_job_param(job, key, buf);
}

JOB_PARAM *get_job_param(PRINT_JOB * job, char *param) {
    JOB_PARAM *res = job->params;
    while (res != NULL) {
        if (!strcmp(param, res->key)) {
            break;
        }
        res = res->next;
    }
    if (res == NULL) {
        res = (JOB_PARAM *) calloc(1, sizeof(JOB_PARAM));
        res->key = malloc(strlen(param) + 1);
        strcpy(res->key, param);
        res->next = job->params;
        job->params = res;
    }
    return res;
}

PRINT_JOB *get_print_job(unsigned int id) {

    PRINT_JOB *res = print_jobs;
    while (res) {
        if (res->id == id) {
            break;
        }
        res = res->next;
    }
    if (res == NULL) {
        res = (PRINT_JOB *) calloc(1, sizeof(PRINT_JOB));
        res->id = id;
        res->new = TRUE;

        if (print_jobs) {
            res->next = print_jobs;
            print_jobs->prev = res;
        }
        print_jobs = res;
        sprintf(buf, "%d", id);
        replace_job_param(res, "JOB_ID", buf);
        replace_job_param(res, LOG_STATUS, LOG_STATUS_UPDATE);
        replace_job_param(res, "AGENT_SPOOLER_HOST", spoolerDnsName);
        replace_job_param(res, "AGENT_VERSION", PRINTLOG_VERSION);

        UUID uuid;
        unsigned char *uuid_str;
        UuidCreate(&uuid);
        UuidToString(&uuid, &uuid_str);
        replace_job_param(res, "JOB_UUID", (char *) uuid_str);
        RpcStringFree(&uuid_str);

        //printf("New job %d :: %ld\n",id,(DWORD)res);
        //print_jobs_queue();
    } else {
        res->new = FALSE;
    };

    return res;
}

int delete_print_job(PRINT_JOB * job) {
    if (job == NULL) {
        return FALSE;
    };
    //printf("Delete job %d :: %ld\n",job->id,(DWORD)job);
    if (job == print_jobs) {
        if (job->prev) {
            print_jobs = job->prev;
        } else if (job->next) {
            print_jobs = job->next;
        } else
            print_jobs = NULL;
    }

    if (job->prev) {
        job->prev->next = job->next;
    }

    if (job->next) {
        job->next->prev = job->prev;
    }

    JOB_PARAM *jparam = job->params;

    while (jparam) {
        JOB_PARAM *prm = jparam;
        if (prm->key) {
            free(prm->key);
        }
        if (prm->value) {
            free(prm->value);
        }

        jparam = prm->next;
        free(prm);
    }
    free(job);
    //print_jobs_queue();
    return TRUE;
}

void report_print_job(PRINT_JOB * job) {
    if (!job) {
        return;
    }
    printf("Report job %d\n", job->id);
    struct curl_httppost *post = NULL;
    struct curl_httppost *last = NULL;

    JOB_PARAM *jparam = job->params;

    while (jparam) {
        JOB_PARAM *prm = jparam;
        printf("\t%s=%s\n", prm->key, prm->value);
        curl_formadd(&post, &last, CURLFORM_COPYNAME, prm->key, CURLFORM_COPYCONTENTS, prm->value, CURLFORM_END);
        jparam = prm->next;
    }
    job->changed = FALSE;

    curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);
    int res = curl_easy_perform(curl);
    if (res != CURLE_OK)
        printf("curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    curl_formfree(post);
}

void init_print_jobs(CURL * reporter) {
    DWORD tempResult;
    curl = reporter;
    HANDLE localSpooler;
    OpenPrinter(NULL, &localSpooler, NULL);
    GetPrinterData(localSpooler,
                   SPLREG_DNS_MACHINE_NAME, NULL, (LPBYTE) spoolerDnsName, sizeof(spoolerDnsName), &tempResult);
    ClosePrinter(localSpooler);
}
