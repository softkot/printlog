#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include "logger.h"

SERVICE_STATUS g_ServiceStatus = { 0 };

SERVICE_STATUS_HANDLE g_StatusHandle = NULL;
HANDLE g_ServiceStopEvent = INVALID_HANDLE_VALUE;

VOID WINAPI ServiceMain(DWORD argc, LPTSTR * argv);
VOID WINAPI ServiceCtrlHandler(DWORD);
DWORD WINAPI ServiceWorkerThread(LPVOID lpParam);

char *SERVICE_NAME = "printlog";

int monitorPrinters(HANDLE stopevent);

void InstallService(char *path, BOOL install) {
    SC_HANDLE hService;         //дескриптор сервиса
    SC_HANDLE hSCManager;       //дескриптор Service Control Manager
    hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE | SC_MANAGER_ALL_ACCESS);
    if (!hSCManager) {
        MessageBox(NULL, "Can't open Service Control Manager", "Error", MB_OK | MB_ICONERROR);
        return;
    }
    if (install) {
        hService = CreateService(hSCManager, SERVICE_NAME,
                                 SERVICE_NAME, SERVICE_ALL_ACCESS,
                                 SERVICE_WIN32_OWN_PROCESS |
                                 SERVICE_INTERACTIVE_PROCESS,
                                 SERVICE_AUTO_START, SERVICE_ERROR_NORMAL, path, NULL, NULL, NULL, NULL, NULL);
        if (!hService) {
            MessageBox(NULL, "Can't create service printlog", "Error", MB_OK | MB_ICONERROR);
            CloseServiceHandle(hSCManager);
            return;
        }
    } else {
        SERVICE_STATUS ss;
        int c = 0;
        hService = OpenService(hSCManager, SERVICE_NAME, SERVICE_STOP | DELETE);
        while (c++ < 5) {
            ControlService(hService, SERVICE_CONTROL_STOP, &ss);
            if (ss.dwCurrentState == SERVICE_STOPPED)
                break;
            Sleep(1000);
        }
        DeleteService(hService);
    };
    CloseServiceHandle(hService);
    CloseServiceHandle(hSCManager);

};

int _tmain(int argc, char *argv[]) {
    SERVICE_TABLE_ENTRY DispatcherTable[] = { {(LPSTR) SERVICE_NAME,
                                               (LPSERVICE_MAIN_FUNCTION)
                                               ServiceMain}
    ,
    {NULL, NULL}
    };

    if (argc > 1) {
        if (!strcmp(argv[1], "/ver")) {
            printf("Version: %s\n", PRINTLOG_VERSION);
            return 0;
        }

        if (!strcmp(argv[1], "/install")) {
            InstallService(argv[0], TRUE);
            return 0;
        }
        if (!strcmp(argv[1], "/uninstall")) {
            InstallService(argv[0], FALSE);
            return 0;
        }
        printf
            ("Usage flags:\n\t/install - installs printlog as system service\n\t/uninstall - deinstalls printlog as system service\n");
        return 0;
    } else {
        if (!StartServiceCtrlDispatcher(DispatcherTable)) {
            printf("Can't run as service. Try to run as user mode process\n");
            monitorPrinters(g_ServiceStopEvent);
            return 0;
        }
    };
    return 0;
}

VOID WINAPI ServiceMain(DWORD argc, LPTSTR * argv) {
    //DWORD Status = E_FAIL;

    printf(_T("printlogservice: ServiceMain: Entry\n"));

    g_StatusHandle = RegisterServiceCtrlHandler(SERVICE_NAME, ServiceCtrlHandler);

    if (g_StatusHandle == NULL) {
        printf(_T("printlogservice: ServiceMain: RegisterServiceCtrlHandler returned error\n"));
        return;
    }
    // Tell the service controller we are starting
    ZeroMemory(&g_ServiceStatus, sizeof(g_ServiceStatus));
    g_ServiceStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    g_ServiceStatus.dwControlsAccepted = 0;
    g_ServiceStatus.dwCurrentState = SERVICE_START_PENDING;
    g_ServiceStatus.dwWin32ExitCode = 0;
    g_ServiceStatus.dwServiceSpecificExitCode = 0;
    g_ServiceStatus.dwCheckPoint = 0;

    if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE) {
        printf(_T("printlogservice: ServiceMain: SetServiceStatus returned error\n"));
    }

    /*
     * Perform tasks neccesary to start the service here
     */
    printf(_T("printlogservice: ServiceMain: Performing Service Start Operations\n"));

    // Create stop event to wait on later.
    g_ServiceStopEvent = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (g_ServiceStopEvent == NULL) {
        printf(_T("printlogservice: ServiceMain: CreateEvent(g_ServiceStopEvent) returned error\n"));

        g_ServiceStatus.dwControlsAccepted = 0;
        g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
        g_ServiceStatus.dwWin32ExitCode = GetLastError();
        g_ServiceStatus.dwCheckPoint = 1;

        if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE) {
            printf(_T("printlogservice: ServiceMain: SetServiceStatus returned error\n"));
        }
        return;
    }
    // Tell the service controller we are started
    g_ServiceStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;
    g_ServiceStatus.dwCurrentState = SERVICE_RUNNING;
    g_ServiceStatus.dwWin32ExitCode = 0;
    g_ServiceStatus.dwCheckPoint = 0;

    if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE) {
        printf(_T("printlogservice: ServiceMain: SetServiceStatus returned error\n"));
    }
    // Start the thread that will perform the main task of the service
    HANDLE hThread = CreateThread(NULL, 0, ServiceWorkerThread, NULL, 0, NULL);

    printf(_T("printlogservice: ServiceMain: Waiting for Worker Thread to complete\n"));

    // Wait until our worker thread exits effectively signaling that the service needs to stop
    WaitForSingleObject(hThread, INFINITE);

    printf(_T("printlogservice: ServiceMain: Worker Thread Stop Event signaled\n"));


    /*
     * Perform any cleanup tasks
     */
    printf(_T("printlogservice: ServiceMain: Performing Cleanup Operations\n"));

    CloseHandle(g_ServiceStopEvent);

    g_ServiceStatus.dwControlsAccepted = 0;
    g_ServiceStatus.dwCurrentState = SERVICE_STOPPED;
    g_ServiceStatus.dwWin32ExitCode = 0;
    g_ServiceStatus.dwCheckPoint = 3;

    if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE) {
        printf(_T("printlogservice: ServiceMain: SetServiceStatus returned error\n"));
    }

    printf(_T("printlogservice: ServiceMain: Exit\n"));

    return;
}

VOID WINAPI ServiceCtrlHandler(DWORD CtrlCode) {
    printf(_T("printlogservice: ServiceCtrlHandler: Entry\n"));

    switch (CtrlCode) {
    case SERVICE_CONTROL_STOP:

        printf(_T("printlogservice: ServiceCtrlHandler: SERVICE_CONTROL_STOP Request\n"));

        if (g_ServiceStatus.dwCurrentState != SERVICE_RUNNING)
            break;

        /*
         * Perform tasks neccesary to stop the service here
         */

        g_ServiceStatus.dwControlsAccepted = 0;
        g_ServiceStatus.dwCurrentState = SERVICE_STOP_PENDING;
        g_ServiceStatus.dwWin32ExitCode = 0;
        g_ServiceStatus.dwCheckPoint = 4;

        if (SetServiceStatus(g_StatusHandle, &g_ServiceStatus) == FALSE) {
            printf(_T("printlogservice: ServiceCtrlHandler: SetServiceStatus returned error\n"));
        }
        // This will signal the worker thread to start shutting down
        SetEvent(g_ServiceStopEvent);
        break;

    default:
        break;
    }
    printf(_T("printlogservice: ServiceCtrlHandler: Exit\n"));
}

DWORD WINAPI ServiceWorkerThread(LPVOID lpParam) {
    printf(_T("printlogservice: ServiceWorkerThread: Entry\n"));
    monitorPrinters(g_ServiceStopEvent);
    printf(_T("printlogservice: ServiceWorkerThread: Exit\n"));
    return ERROR_SUCCESS;
}
